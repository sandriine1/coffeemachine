package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

import main.Chocolate;
import main.Coffee;
import main.Commande;
import main.Message;
import main.OrangeJuice;
import main.StockMachine;
import main.Tea;

class CoffeeMachineTest {

	@Test
	void testCommandeTeaWithSugar() {
		Commande c1 = new Commande(new Tea(), 1);
		c1.ajoutStick();
		String order1 = c1.toString();
		assertEquals("T:1:0", order1);
	}
	
	@Test
	void testCommandeChocolatWithoutSugar() {
		Commande c2 = new Commande(new Chocolate());
		c2.ajoutStick();
		String order2 = c2.toString();
		assertEquals("H::", order2);
	}
	
	@Test
	void testMessage() {
		Message m = new Message("message-content");
		assertEquals("M:message-content", m.toString());
	}
	
	@Test
	void testCommandeTeaWith40Cents() {
		Commande c3 = new Commande(new Tea());
		String order = c3.passerCommande(0.40);
		assertEquals("T::", order);
	}
	
	@Test
	void testCommandeTeaWith20Cents() {
		Commande c4 = new Commande(new Tea());
		String order = c4.passerCommande(0.20);
		assertEquals("M:Missing 20 cents", order);
	}
	
	@Test
	void testCommandeOrangeJuice() {
		Commande c5 = new Commande(new OrangeJuice());
		String order = c5.passerCommande(0.60);
		assertEquals("O::", order);
	}
	
	@Test
	void testCommandeHotCoffee() {
		Commande c6 = new Commande(new Coffee());
		c6.reglerTemperature("h");
		String order = c6.passerCommande(0.60);
		assertEquals("Ch::", order);
	}
	
	@Test
	void testCommandeHotChocolateWithSugar() {
		Commande c6 = new Commande(new Chocolate(), 2);
		c6.reglerTemperature("h");
		String order = c6.passerCommande(0.50);
		assertEquals("Hh:2:0", order);
	}
	
	@Test
	void testCommandeHotTeaWithSugar() {
		Commande c6 = new Commande(new Tea(), 2);
		c6.reglerTemperature("h");
		String order = c6.passerCommande(0.40);
		assertEquals("Th:2:0", order);
	}
	
	@Test
	void testCalculStockAfterCommandeOneCoffee() {
		Map<String, Integer> boissons = new HashMap<String, Integer>(); 
	    boissons.put("C", 20);
		StockMachine s = new StockMachine(boissons, 0);
		//Passage de la commande avec calcul des gain et maj du stock
		Commande c10 = new Commande(new Coffee(), 1);
		c10.passerCommandeEtMajLeStock(0.6, s);
		assertEquals(1, s.getBoissonsVendues().get("C"));
	}
	
	@Test
	void testCalculGainAfterTwoCommandes() {
		Map<String, Integer> boissons = new HashMap<String, Integer>(); 
	    boissons.put("C", 20);
	    boissons.put("H", 10);
		StockMachine s = new StockMachine(boissons, 0);
		//Passage de la commande avec calcul des gain et maj du stock
		Commande c10 = new Commande(new Coffee(), 1);
		c10.passerCommandeEtMajLeStock(0.6, s);
		Commande c11 = new Commande(new Chocolate());
		c11.passerCommandeEtMajLeStock(0.5, s);
		assertEquals(1.1, s.getMontantGain());
	}
	
	@Test
	void testMessageBoissonEmpty() {
		Map<String, Integer> boissons = new HashMap<String, Integer>(); 
	    boissons.put("O", 1);
		StockMachine s = new StockMachine(boissons, 0);
		//Passage de la commande avec calcul des gain et maj du stock
		Commande c13 = new Commande(new OrangeJuice());
		c13.passerCommandeEtMajLeStock(0.6, s);
		Commande c14 = new Commande(new OrangeJuice());
		String message = c13.passerCommandeEtMajLeStock(0.6, s);
		assertEquals("D�sol� il y a une p�nurie de O, un message a �t� envoy� au centre de service.", message);
	}
	


}
